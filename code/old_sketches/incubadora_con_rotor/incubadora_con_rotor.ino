/*
  Fade

  This example shows how to fade an LED on pin 9 using the analogWrite()
  function.

  The analogWrite() function uses PWM, so if you want to change the pin you're
  using, be sure to use another PWM capable pin. On most Arduino, the PWM pins
  are identified with a "~" sign, like ~3, ~5, ~6, ~9, ~10 and ~11.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Fade

R
> i = function(Res,Vlt=12) Vlt/Res
> w = function(Res,Vlt=12) Res * i(Res)^2
> w(Res=8)
[1] 18
> w(Res=16)
[1] 9

*/

int motor = 9;         // the PWM pin the MOTOR is attached to
int heater = 10;       // the PWM pin the HEATER is attached to
int speed;    // variable, MOTOR speed signal

// Include the libraries we need
#include <OneWire.h>
#include <DallasTemperature.h>

// Data wire is plugged into port 2 on the Arduino
#define ONE_WIRE_BUS 2

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

/********************************************************
 * PID Basic Example
 * Reading analog input 0 to control analog PWM output 3
 ********************************************************/

#include <PID_v1.h>

//Define Variables we'll be connecting to
double Setpoint, Input, Output;
//Specify the links and initial tuning parameters
double Kp=2, Ki=5, Kd=1;
// Initialize PID
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);


// the setup routine runs once when you press reset:
void setup() {
  // declare pin 9 to be an output:
  pinMode(motor, OUTPUT);
  pinMode(heater, OUTPUT);

  // Set initial speed to zero
  speed = 0;

  // start serial port
  Serial.begin(9600);
  Serial.println("Input");
  Serial.print("\t");
  Serial.println("Output");

  // Start up the One-wire library
  sensors.begin();

  // Initialize the variables PID is linked to:
  // Input
  sensors.requestTemperatures();
  Input = sensors.getTempCByIndex(0);
  // Setpoint
  Setpoint = 28;
  
  // turn the PID on
  myPID.SetMode(AUTOMATIC);
}

// the loop routine runs over and over again forever:
void loop() {
  sensors.requestTemperatures();
  Input = sensors.getTempCByIndex(0);
  myPID.Compute();
  analogWrite(heater, Output);
  analogWrite(motor, Output);

  Serial.println(Input);
  Serial.print("\t");
  Serial.println(Output);
}
