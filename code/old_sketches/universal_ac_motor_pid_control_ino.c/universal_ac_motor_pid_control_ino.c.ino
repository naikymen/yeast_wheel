// PID speed control:
// https://www.instructables.com/id/Make-your-own-Lathe-from-other-peoples-rubbish/
// universal_ac_motor_pid_control_ino.c.ino
#include <avr/io.h>
#include <avr/interrupt.h>

// Tachometer (hall effect sensor) stuff
#define TACHO 3            // tacho signals input pin (either 2 or 3 in the Arduino Uno)
#define TACHOPULSES 1      // number of pulses per revolution

// RPM counting variables
float RPM;                   // real rpm variable
unsigned int count;                 // tacho pulses count variable
unsigned int lastcount = 0;         // additional tacho pulses count variable
unsigned long lastcounttime = 0;
//unsigned long lastflash;
unsigned long lastpiddelay = 0;
unsigned long previousMillis = 0;
const int rpmReportInterval = 2000;  // rpm report interval, in ms

// RPM counting routine
void tacho() {
  count++;
}

// RPM averaging
// https://www.arduino.cc/en/Tutorial/BuiltInExamples/Smoothing
const int numReadings = 3;
float readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
float total = 0;                  // the running total
float averageRPM = 0;                // the average

void setup() {
  Serial.begin(9600);

  // Set-up interrupt pins
  // https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
  pinMode(TACHO, INPUT);              // set the tacho pulses detect pin // ¿pinMode(interruptPin, INPUT_PULLUP);?
  // set up tacho sensor interrupt IRQ1 on pin3
  attachInterrupt(digitalPinToInterrupt(TACHO), tacho, FALLING);

  // Initialize RPM averaging
  // initialize all the readings to 0:
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings[thisReading] = 0;
  }
}

void loop(){
    // After 1 second of counting pulses:
    unsigned long counttime = millis();
    if (counttime - lastcounttime >= rpmReportInterval) {
        // record the counts in "lastcount"
        lastcount = count;
        // and reset count to zero
        count = 0;

        // Compute and print RPM:
        float prerpm = 60 * 1000 * count / rpmReportInterval;
        RPM = prerpm / TACHOPULSES;

        // Averaging RPM
        // subtract the previous reading fron total:
        total = total - readings[readIndex];
        // read from the sensor, and replace the reading we just subtracted with the new one:
        readings[readIndex] = RPM;
        // now add the new reading to the total:
        total = total + readings[readIndex];
        // and advance to the next position in the array:
        readIndex = readIndex + 1;

        // if we're at the end of the array...
        if (readIndex >= numReadings) {
            // ...wrap around to the beginning:
            readIndex = 0;
        }

        // calculate the average:
        averageRPM = total / numReadings;
        // send it to the computer as ASCII digits
        Serial.println(averageRPM);

        // reset last count time to the present
        lastcounttime = millis();
    }

}



