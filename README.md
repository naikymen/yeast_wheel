# Descripcion mínima

- [ ] Mejorar documentacion.

* Arduino + PID
  * Control de temperatura con resistencias y un termómetro.
  * Control de velocidad de rotación con un hall sensor.
* Partes i3D diseñadas en freecad.
* Estructura de madera.
* Varillas de 8 mm y rulemanes.
* Tornillos M3.

![v0](doc/v0.1.png)

# Code

See Arduino sketch at: `code/incubadora_rotor_pid/incubadora_rotor_pid.ino`

# Misc

Como importar un STL como solid en FreeCAD:

* https://wiki.freecadweb.org/Import_from_STL_or_OBJ

Pendiente:

- [x] Agregar sockets para un par de rulemanes, que sostengan el rotor en la direccion del eje de rotacion. Actualmente están sostenidos por el herringbone gear solamente (y no da).
